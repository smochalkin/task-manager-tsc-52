package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.service.IAuthService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.util.HashUtil;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final UserDto userDto = userService.findByLogin(login);
        if (userDto.isLock()) throw new UserIsLocked();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!userDto.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        userId = userDto.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Override
    @NotNull
    public UserDto getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void checkRoles(Role @Nullable ... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final UserDto userDto = getUser();
        @Nullable final Role role = userDto.getRole();
        for (Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
